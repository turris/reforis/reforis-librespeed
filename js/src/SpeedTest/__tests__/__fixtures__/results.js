/*
 * Copyright (C) 2020-2023 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

const resultsFixture = {
    performed_tests: [
        {
            ping: 1.0,
            jitter: 0.1,
            server: "Prague, Czech Republic (CESNET)",
            speed_download: 663.07,
            speed_upload: 35.55,
            time: "2023-02-09T19:44:07.642646278+01:00",
        },
        {
            ping: 7.91,
            jitter: 0.2,
            server: "Prague, Czech Republic (CESNET)",
            speed_download: 664.07,
            speed_upload: 35.55,
            time: "2023-02-09T19:45:07.642646278+01:00",
        },
        {
            ping: 15.91,
            jitter: 0.3,
            server: "Prague, Czech Republic (CESNET)",
            speed_download: 800.07,
            speed_upload: 35.55,
            time: "2023-02-09T19:46:07.642646278+01:00",
        },
    ],
};

export default resultsFixture;
